import os
import sys
import datetime
import dateutil.parser
import configparser
import re
import click
import CloudFlare
import apprise

import mysql.connector
from mysql.connector import Error

VERSION = 0.3
pages = 500
update_days = 28


def getConfig():

    cp = configparser.ConfigParser()
    try:
        cp.read([
            'config.ini',
            'app/config.ini',
            '/etc/zoneinfo/config.ini',  # put it here for live
            '.config.ini'
        ])
    except Error:
        raise Exception("%s: config file not found" % ('config.ini'))

    if len(cp.sections()) == 0:
        raise Exception("%s: Config file error")
    else:
        if cp.has_section('mysql'):
            if cp.has_option('mysql', 'socket'):
                socket = cp.get('mysql', 'socket')
            else:
                socket = None
            if cp.has_option('mysql', 'hostname'):
                hostname = cp.get('mysql', 'hostname')
            else:
                hostname = None
                user = cp.get('mysql', 'user')
                password = cp.get('mysql', 'password')
                database = cp.get('mysql', 'database')

        if cp.has_section('cloudflare'):
            cfname = cp.get('cloudflare', 'name')

        if cp.has_section('apprise'):
            notify_url = cp.get('apprise', 'notify_url')

        if hostname:
            config = {'hostname': hostname, 'user': user,
                      'password': password, 'database': database, 'cfname': cfname, 'notify_url': notify_url}
        else:
            config = {'socket': socket, 'user': user,
                      'password': password, 'database': database, 'cfname': cfname, 'notify_url': notify_url}

        return config


def cfConnect():
    config = getConfig()
    if config['cfname']:
        cfname = config['cfname']
        cf = CloudFlare.CloudFlare(profile=cfname)
        return cf
    else:
        exit("Cloudflare config not found")


def cfDomainQuery(domain):

    cf = cfConnect()

    zone_info = []
    try:
        zones = cf.zones.get(params={'name': domain})
    except CloudFlare.exceptions.CloudFlareAPIError as e:
        exit('/zones.get %d %s - api call failed' % (e, e))

    zc = len(zones)

    if zc == 0:
        exit('no zones found')
    else:
        zone_data = {}

    zone_id = zones[0]['id']
    account = zones[0]['account']
    account_id = account['id']
    zone_data = {
        'zone_id': zone_id,
        'account': account,
        'account_id': account_id,
    }

    bare_name = domain
    www_name = 'www' + '.' + domain
    try:
        zone_records = cf.zones.dns_records.get(zone_id)
    except CloudFlare.exceptions.CloudFlareAPIError as e:
        exit('error', e)

    zr = len(zone_records)

    if zr == 0:
        click.echo('No records for %s' % domain)
    else:
        record_data = {}
        x = 0
        for record in zone_records:
            if record['name'] == bare_name and record['type'] == 'A':
                record_data[x] = record
            elif record['name'] == www_name and record['type'] == 'CNAME':
                record_data[x] = record
            elif record['name'] == bare_name and record['type'] == 'CNAME':
                record_data[x] = record
            elif record['name'] == www_name and record['type'] == 'A':
                record_data[x] = record
            x = x + 1

    zone_info.append(zone_data)
    zone_info.append(record_data)
    return zone_info


def dbConnect():
    config = getConfig()
    user = config.get('user')
    password = config.get('password')
    db = config.get('database')
    if config.get('hostname') is None:
        socket = config.get('socket')
        is_socket = True
    else:
        hostname = config.get('hostname')

    if is_socket:
        try:
            connection = mysql.connector.connect(
                unix_socket=socket, user=user, passwd=password, db=db)
        except mysql.connector.Error as e:
            exit(e)
    else:
        try:
            connection = mysql.connector.connect(
                host=hostname, user=user, passwd=password, db=db)
        except mysql.connector.Error as e:
            exit(e)

    if connection.is_connected():
        return connection


def dbSelect(sql_query):

    connection = dbConnect()
    cursor = connection.cursor(dictionary=True)

    try:
        cursor.execute(sql_query)
    except Error as e:
        click.echo('Query failed: %s' % e)

    results = cursor.fetchall()

    return results


def dbUpdate(sql_query, update_data):
    connection = dbConnect()
    cursor = connection.cursor(dictionary=True)
    try:
        results = cursor.execute(sql_query, update_data)
        connection.commit()
    except Error as e:
        results = ('Query failed: %s' % e)
    return results


def updateRecord(record_type, record_ip, record_update, record_id):
    """ Update a record """
    sql_query = (
        """UPDATE zonerecords SET hosttype=%s, original_ip=%s, last_update=%s WHERE record_id=%s;""")
    update_data = (record_type, record_ip, record_update, record_id)
    results = dbUpdate(sql_query, update_data)


def deleteRecord(hostname):
    """ Delete a record """
    sql_query = (
        """DELETE from zonerecords WHERE hostname=%s""")
    update_data = (hostname,)
    results = dbUpdate(sql_query, update_data)


def deleteDomain(domain):
    sql_query = (
        """DELETE zoneinfo, zonerecords from zoneinfo INNER JOIN zonerecords WHERE zoneinfo.domain = zonerecords.domain AND zoneinfo.domain = %s""")
    update_data = (domain,)
    results = dbUpdate(sql_query, update_data)
    click.echo("Domain %s deleted" % (domain))


def Notify():
    config = getConfig()
    if config['notify_url']:
        notify_url = config.get('notify_url')
        apobj = apprise.Apprise()
        apobj.add(notify_url)
        return apobj
    else:
        error('Notification URL not found')


def sendNotification(notify_body):
    apobj = Notify()
    apobj.notify(body=notify_body)


@ click.group()
def cli():
    """ Get Cloudflare customer domain information """


@ cli.command('domain')
@ click.argument('domain')
def getDomainInfo(domain):
    """ Get a domain's Cloudflare account holder """

    # zone_info = cfDomainQuery(domain)
    cf = cfConnect()

    sql_query = ("""SELECT * from zoneinfo WHERE domain = '%s';""" % domain)

    results = dbSelect(sql_query)

    if len(results) == 0:
        click.echo('Record not found in database, retrieving from Cloudflare')
        zone_info = cfDomainQuery(domain)
        zone_data = zone_info[0]
        account_data = zone_data['account']
        account_name = account_data['name']
    else:
        for result in results:
            account_name = result['account_name']
    click.echo(account_name)


@ cli.command('records')
@ click.argument('domain')
def cfGetRecords(domain):
    """ Get bare and www host records from a domain """

    cf = cfConnect()

    sql_query = ("SELECT * from zonerecords WHERE domain = '%s';" % (domain))

    results = dbSelect(sql_query)

    rc = len(results)

    if rc >= 1:
        host_records = []
        for row in results:
            hostname = row['hostname']
            hosttype = row['hosttype']
            original_ip = row['original_ip']
            last_update = row['last_update']
            # record_data = [hostname, hosttype, original_ip, last_update]

            click.echo('Hostname: %s, Type: %s, IP: %s, Last update: %s' %
                       (hostname, hosttype, original_ip, last_update))

    else:

        if len(results) == 0:
            click.echo(
                'Record not found in database, retrieving from Cloudflare')
            zone_info = cfDomainQuery(domain)
            zone_data = zone_info[0]
            account_data = zone_data['account']
            account_name = account_data['name']

            record_data = zone_info[1]

            rd = len(record_data)

            for x in record_data:
                hostname = record_data[x]['name']
                hosttype = record_data[x]['type']
                content = record_data[x]['content']
                modified = record_data[x]['modified_on']

                click.echo('Hostname: %s, Type: %s, IP: %s, Last update: %s' %
                           (hostname, hosttype, content, modified))


@ cli.command('check')
@ click.argument('domain')
@ click.option('--update', 'modify', flag_value='update')
@ click.option('--notify', default=False)
@ click.option('--verbose', default=False)
def checkDomainInfo(domain, modify, notify, verbose):
    """ Check for changes in domain table """

    cf = cfConnect()

    if domain == 'all':
        sql_query = (
            "SELECT * from zoneinfo.zonerecords r LEFT JOIN zoneinfo.zoneinfo z ON r.domain = z.domain;")
    else:
        sql_query = (
            "SELECT * from zoneinfo.zonerecords r LEFT JOIN zoneinfo.zoneinfo z ON r.domain = z.domain WHERE z.domain = '%s';" % (domain))

    results = dbSelect(sql_query)

    record_count = 0  # check there is a result

    for result in results:
        zone_id = result['zone_id']
        zone_record_id = result['record_id']
        account_name = result['account_name']
        zone_domain = result['domain']
        hostname = result['hostname']
        hosttype = result['hosttype']
        local_ip = result['original_ip']
        last_update = result['last_update']
        # trying to normalise the timestamp
        last_update = last_update.strftime("%Y-%m-%d %H:%M:%S")

        if verbose:
            print("Checking %s" % (hostname))

        zone_records = None

        try:
            zone_records = cf.zones.dns_records.get(zone_id)
        except CloudFlare.exceptions.CloudFlareAPIError as e:
            if verbose:
                print("Zone can't be retrieved: %s" % e)
            notify_body = (
                "Zone %s can't be retrieved, consider deleting from database" % zone_domain)
            sendNotification(notify_body)

        if zone_records is not None:

            for record in zone_records:
                record_id = record['id']
                if record_id == zone_record_id:
                    record_count = record_count + 1
                    record_ip = record['content']
                    record_type = record['type']
                    record_update = record['modified_on']
                    record_iso = dateutil.parser.isoparse(record_update)
                    record_update = record_iso.strftime("%Y-%m-%d %H:%M:%S")
                    if record_update == last_update:
                        next
                    else:
                        # check if the things we're interested in actually change
                        if record_ip == local_ip and record_type == hosttype:
                            next
                        else:
                            notify_body = ("Record %s has changed: Local IP: %s | CloudFlare IP: %s | Local type: %s | CloudFlare type: %s" % (
                                hostname, local_ip, record_ip, hosttype, record_type))
                            sendNotification(notify_body)

                            if modify == 'update':
                                results = updateRecord(
                                    record_type, record_ip, record_update, record_id)
                                notify_body = (
                                    "Record %s updated" % (hostname))
                                sendNotification(notify_body)

        else:
            notify_body = ("Zone record %s not found - check account %s" %
                           (hostname, account_name))
            if notify:
                sendNotification(notify_body)
                print(notify_body)

    # if a record has been removed from Cloudflare there shouldn't be a count.
    if record_count == 0:
        notify_body = ('Record not found for %s' % (domain))
        sendNotification(notify_body)
        print(notify_body)


@ cli.command('delete')
@ click.argument('domain')
def deleteDomainInfo(domain):
    """ Delete a domain """
    result = deleteDomain(domain)
    print(result)


@ cli.command('install')
def installDomainInfo():
    """ Populate domain tables """

    cf = cfConnect()

    sql_query = ("SELECT domain from zoneinfo;")

    results = dbSelect(sql_query)

    numrows = len(results)

    if numrows == 0:
        click.echo('Populating database from Cloudflare')
        zone_inserts = []
        host_inserts = []

        try:
            all_zones = cf.zones.get(params={'per_page': pages})
        except CloudFlare.exceptions.CloudFlareAPIError as e:
            exit('/zones.get %d %s - api call failed' % (e, e))

        for zone in all_zones:
            domain = zone.get('name')
            click.echo('Getting %s' % domain)
            account = zone.get('account')
            account_name = account.get('name')
            account_id = account.get('id')
            record_id = zone.get('id')
            last_update = zone.get('modified_on')
            last_isoformat = dateutil.parser.isoparse(last_update)
            last_datetime = last_isoformat.strftime("%Y-%m-%d %H:%M:%S")

            account_name = re.sub(r"([\' \@])", r'\\\1', account_name)

            add_domain = ("INSERT INTO zoneinfo (domain, account_name, account_id, last_update) VALUES ('%s', '%s', '%s', '%s')" % (
                domain, account_name, account_id, last_datetime))
            zone_inserts.append(add_domain)

            try:
                domain_records = cf.zones.get(params={'name': domain})
            except CloudFlare.exceptions.CloudFlareAPIError as e:
                print('/zones.get %d %s - api call failed' % (e, e))

            if len(domain_records) == 0:
                click.echo('No records found')
            else:

                for domain_record in domain_records:

                    zone_id = zone['id']

                    bare_name = domain
                    www_name = 'www' + '.' + domain
                    try:
                        zone_records = cf.zones.dns_records.get(zone_id)
                    except CloudFlare.exceptions.CloudFlareAPIError as e:
                        exit('error', e)

                    click.echo('Processing zone records')
                    for zone_record in zone_records:
                        record_data = []
                        if zone_record['name'] == bare_name and zone_record['type'] == 'A':
                            record_data.append(zone_record)
                        elif zone_record['name'] == www_name and zone_record['type'] == 'CNAME':
                            record_data.append(zone_record)
                        elif zone_record['name'] == bare_name and zone_record['type'] == 'CNAME':
                            record_data.append(zone_record)
                        elif zone_record['name'] == www_name and zone_record['type'] == 'A':
                            record_data.append(zone_record)

                        for host_record in record_data:
                            hostname = host_record['name']
                            hosttype = host_record['type']
                            zone_id = host_record['zone_id']
                            original_ip = host_record['content']
                            record_id = host_record['id']
                            # normalise the datetime
                            last_update = host_record['modified_on']
                            last_isoformat = dateutil.parser.isoparse(
                                last_update)
                            last_datetime = last_isoformat.strftime(
                                "%Y-%m-%d %H:%M:%S")
                            click.echo('Inserting %s' % hostname)
                            add_record = ("INSERT into zonerecords (domain, hostname, hosttype, zone_id, record_id, original_ip, account_id, last_update) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (
                                domain, hostname, hosttype, zone_id, record_id, original_ip, account_id, last_datetime))
                            host_inserts.append(add_record)

        try:
            zone_cursor = connection.cursor(buffered=True, dictionary=True)
        except mysql.connector.Error as e:
            click.echo("Error while connecting to MySQL", e)

        try:
            host_cursor = connection.cursor(buffered=True, dictionary=True)
        except mysql.connector.Error as e:
            click.echo("Error while connecting to MySQL", e)

        click.echo('Inserting domains')
        for zone in zone_inserts:
            try:
                zone_cursor.execute(zone)
            except mysql.connector.Error as e:
                click.echo('Database error %s' % e)

        click.echo('Inserting hosts')
        for host in host_inserts:
            try:
                host_cursor.execute(host)
            except mysql.connector.Error as e:
                click.echo('Database error %s' % e)

        connection.commit()

    connection.close()
    click.echo('Population complete')


@ cli.command('createdb')
def createdb():
    """ Create databases if not created """

    # config = getConfig()

    if config.get('hostname') is None:
        socket = config.get('socket')
        is_socket = True
    else:
        hostname = config.get('hostname')
    user = config.get('user')
    password = config.get('password')
    db = config.get('database')
    cfname = config.get('cfname')

    if is_socket:
        connection = mysql.connector.connect(
            unix_socket=socket, user=user, passwd=password, db=db)
    else:
        connection = mysql.connector.connect(
            host=hostname, user=user, passwd=password, db=db)

    if connection.is_connected:
        sql = ["CREATE TABLE IF NOT EXISTS zoneinfo (id int(11) AUTO_INCREMENT PRIMARY KEY, domain varchar(128), account_name varchar(128), account_id varchar(64), last_update DATETIME);",
               "CREATE TABLE IF NOT EXISTS zonerecords (id int(11) AUTO_INCREMENT PRIMARY KEY, domain varchar(128), hostname varchar(128), hosttype varchar(8), zone_id varchar(64), record_id varchar(64), original_ip varchar(64), account_id varchar(64), last_update DATETIME);"]
        cursor = connection.cursor()
        for i in sql:
            try:
                cursor.execute(i)
            except Error as e:
                print(e)
                exit("Error while connecting to MySQL", e)

        click.echo('Database created')


if __name__ == '__main__':
    cli()
