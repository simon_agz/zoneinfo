from setuptools import setup

setup(
    name='zoneinfo',
    version='0.0.1',
    install_requires=[
        'click',
        'CloudFlare',
        'datetime.parser'
    ],
    entry_points={
        'console_scripts': [
            'zoneinfo = zoneinfo.bin.zoneinfo:cli',
        ],
    },
)